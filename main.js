var app = new Vue({
    el: '#app',
    data: {
        product: 'FunKo Pop Jon Snow',
        description: 'Figure representing Jon Snow, a character from Game Of Thrones, the famous HBO tv show.',
        image: './assets/funko-jonsnow.png',
        titleImg: 'FunKo Pop Jon Snow',
        altImg: 'Jon Snow',
        inventory: 1,
        onSale: true,
        details: [
            'From GOT, Jon Snow, as a stylized POP vinyl from Funko!',
            'Stylized collectable stands 3¾ inches tall, perfect for any GOT fan!',
            'Collect and display all GOT POP! Vinyl\'s!'
        ]
    }
})
